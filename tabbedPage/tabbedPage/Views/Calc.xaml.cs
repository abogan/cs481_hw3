﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbedPage.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Calc : ContentPage
    {
        string currOp = "None";
        int firstArg = -1;
        int secondArg = -1;
        int finalResult = -1;
        
        async void OnCalcAppear(Object sender, EventArgs e)
        {
            //for displaying popups: https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/pop-ups
            await DisplayAlert("Hello", "This simple calculator allows multiplication, subtraction, division, and addition. ", "OK");
        }
        
        void Clear(Object sender, EventArgs e)
        {
            //reset all variables and text field
            theResult.Text = "";
            firstArg = -1;
            secondArg = -1;
            finalResult = -1;
        }
        void EqualsPressed(Object sender, EventArgs e)
        {
            if (firstArg != -1 && theResult.Text != "")
            {
                secondArg = Int32.Parse(theResult.Text);
                if (currOp == "Add")
                    finalResult = firstArg + secondArg;
                else if (currOp == "Sub")
                    finalResult = firstArg - secondArg;
                else if (currOp == "Div")
                    finalResult = firstArg / secondArg;
                else if (currOp == "Mul")
                    finalResult = firstArg * secondArg;

                //set text to result and reset all variables
                theResult.Text = finalResult.ToString();
                firstArg = -1;
                secondArg = -1;
                finalResult = -1;
            }
        }

        void PlusPressed(Object sender, EventArgs e)
        {
            currOp = "Add";
            //if firstArg hasn't been edited and the input field isn't blank, 
            //set contents of input field to firstArg
            if (firstArg == -1 && theResult.Text != "")
            {
                //C#: string to int 
                //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/how-to-convert-a-string-to-a-number
                firstArg = Int32.Parse(theResult.Text);
                //clear text field
                theResult.Text = "";
            }
            //if firstArg has been edited and input field isn't blank,
            //add contents of input field to firstArg
            else if (firstArg != -1 && theResult.Text != "")
            {
                firstArg += Int32.Parse(theResult.Text);
                theResult.Text = "";
            }
        }
        void MinusPressed(Object sender, EventArgs e)
        {
            currOp = "Sub";
            //if firstArg hasn't been edited and the input field isn't blank, 
            //set contents of input field to firstArg
            if (firstArg == -1 && theResult.Text != "")
            {
                //C#: string to int 
                //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/how-to-convert-a-string-to-a-number
                firstArg = Int32.Parse(theResult.Text);
                //clear text field
                theResult.Text = "";
            }

            //if firstArg has been edited and input field isn't blank,
            //subtract contents of input field from firstArg
            else if (firstArg != -1 && theResult.Text != "")
            {
                firstArg -= Int32.Parse(theResult.Text);
                theResult.Text = "";
            }
        }

        void DivPressed(Object sender, EventArgs e)
        {
            currOp = "Div";
            //if firstArg hasn't been edited and the input field isn't blank, 
            //set contents of input field to firstArg
            if (firstArg == -1 && theResult.Text != "")
            {
                //C#: string to int 
                //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/how-to-convert-a-string-to-a-number
                firstArg = Int32.Parse(theResult.Text);
                //clear text field
                theResult.Text = "";
            }

            //if firstArg has been edited and input field isn't blank,
            //divide: firstArg/input value
            else if (firstArg != -1 && theResult.Text != "")
            {
                firstArg /= Int32.Parse(theResult.Text);
                theResult.Text = "";
            }
        }

        void MulPressed(Object sender, EventArgs e)
        {
            currOp = "Mul";
            //if firstArg hasn't been edited and the input field isn't blank, 
            //set contents of input field to firstArg
            if (firstArg == -1 && theResult.Text != "")
            {
                //C#: string to int 
                //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/how-to-convert-a-string-to-a-number
                firstArg = Int32.Parse(theResult.Text);
                //clear text field
                theResult.Text = "";
            }

            //if firstArg has been edited and input field isn't blank,
            //multiply firstArg*input value
            else if (firstArg != -1 && theResult.Text != "")
            {
                firstArg *= Int32.Parse(theResult.Text);
                theResult.Text = "";
            }
        }

        void ZeroPressed(Object sender, EventArgs e)
        {
            theResult.Text += "0";
        }
        void OnePressed(Object sender, EventArgs e)
        {
            theResult.Text += "1";
        }
        void TwoPressed(Object sender, EventArgs e)
        {
            theResult.Text += "2";
        }
        void ThreePressed(Object sender, EventArgs e)
        {
            theResult.Text += "3";
        }
        void FourPressed(Object sender, EventArgs e)
        {
            theResult.Text += "4";
        }
        void FivePressed(Object sender, EventArgs e)
        {
            theResult.Text += "5";
        }
        void SixPressed(Object sender, EventArgs e)
        {
            theResult.Text += "6";
        }
        void SevenPressed(Object sender, EventArgs e)
        {
            theResult.Text += "7";
        }
        void EightPressed(Object sender, EventArgs e)
        {
            theResult.Text += "8";
        }
        void NinePressed(Object sender, EventArgs e)
        {
            theResult.Text += "9";
        }
        public Calc()
        {
            InitializeComponent();
        }
    }
}