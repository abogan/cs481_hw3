﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbedPage.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class tapASquare : ContentPage
    {
        
        async void OnSquareDisappear(Object sender, EventArgs e)
        {
            await DisplayAlert("Goodbye", "The colors of your squares will be saved when you return to the third tab. ", "OK");
        }
        //returns a random color when a button is pressed
        private Xamarin.Forms.Color returnRandomColor()
        {
            //generating random color in C#: 
            //https://stackoverflow.com/questions/5805774/how-to-generate-random-color-names-in-c-sharp
            Random ranNum = new Random();
            return Color.FromRgb(ranNum.Next(255), ranNum.Next(255), ranNum.Next(255));
        }
        private void Button1(object sender, EventArgs e)
        {
            button1.BackgroundColor = returnRandomColor();
        }
        private void Button2(object sender, EventArgs e)
        {
            button2.BackgroundColor = returnRandomColor();
        }
        private void Button3(object sender, EventArgs e)
        {
            button3.BackgroundColor = returnRandomColor();
        }
        private void Button4(object sender, EventArgs e)
        {
            button4.BackgroundColor = returnRandomColor();
        }
        private void Button5(object sender, EventArgs e)
        {
            button5.BackgroundColor = returnRandomColor();
        }
        private void Button6(object sender, EventArgs e)
        {
            button6.BackgroundColor = returnRandomColor();
        }
        private void Button7(object sender, EventArgs e)
        {
            button7.BackgroundColor = returnRandomColor();
        }
        private void Button8(object sender, EventArgs e)
        {
            button8.BackgroundColor = returnRandomColor();
        }
        private void Button9(object sender, EventArgs e)
        {
            button9.BackgroundColor = returnRandomColor();
        }
        private void Button10(object sender, EventArgs e)
        {
            button10.BackgroundColor = returnRandomColor();
        }
        private void Button11(object sender, EventArgs e)
        {
            button11.BackgroundColor = returnRandomColor();
        }
        private void Button12(object sender, EventArgs e)
        {
            button12.BackgroundColor = returnRandomColor();
        }
        private void Button13(object sender, EventArgs e)
        {
            button13.BackgroundColor = returnRandomColor();
        }
        private void Button14(object sender, EventArgs e)
        {
            button14.BackgroundColor = returnRandomColor();
        }
        private void Button15(object sender, EventArgs e)
        {
            button15.BackgroundColor = returnRandomColor();
        }
        private void Button16(object sender, EventArgs e)
        {
            button16.BackgroundColor = returnRandomColor();
        }
        
        public tapASquare()
        {
            InitializeComponent();
        }
    }
}