﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel; //used for ObservableCollection
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbedPage.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class Office : ContentPage
    {

        //list of employees 
        ObservableCollection<string> employees = new ObservableCollection<string>();

        
        async void OnOfficeDisappear(Object sender, EventArgs e)
        {
            await DisplayAlert("Goodbye", "Your list will be saved when you return to the second tab. ", "OK");
        }

        //adds user input to list of employees
        public void Handle_AddButtonPressed(object sender, EventArgs e)
        {
            employees.Add(entryText.Text);
            theEmpList.ItemsSource = employees;
        }
        //removes last entry from list of employees
        public void Handle_RemoveButtonPressed(object sender, EventArgs e)
        {
            if (employees.Count > 0)
            {
                employees.RemoveAt(employees.Count - 1);
                theEmpList.ItemsSource = employees;
            }

        }
        public Office()
        {
            InitializeComponent();
        }
    }
}